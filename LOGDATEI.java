import java. io. *;

/**
 * Verwaltet die Eintr&auml;ge in die Logdatei.
 * 
 * @author Albert Wiedemann
 * @version 1.0
 */
class LOGDATEI implements MELDUNGSBEOBACHTER
{
    /** Zeichenstrom zur Ausgabedatei */
    BufferedWriter writer;

    /**
     * &Ouml;ffnet die Logdatei.
     */
    LOGDATEI ()
    {
        try
        {
            writer = new BufferedWriter (new FileWriter ("log.dat", true));
        }
        catch (Exception e)
        {
            e. printStackTrace ();
        }
    }

    /**
     * Schliesst die Logdatei.
     */
    void Schliessen ()
    {
        try
        {
            writer. close ();
        }
        catch (Exception e)
        {
            e. printStackTrace ();
        }
    }

    /**
     * Fehlermeldungen beim Ausf&uuml;hren von Aktionen
     */
    public void FehlermeldungEmpfangen (String text)
    {
        // nichts
    }

    /**
     * Aktionslog f&uuml;r den Ablauf
     */
    public void LogeintragEmpfangen (String text)
    {
        try
        {
            writer. write (text);
            writer. newLine ();
        }
        catch (Exception e)
        {
            e. printStackTrace ();
        }
    }
}
