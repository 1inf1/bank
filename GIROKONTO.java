
/**
 * Beschreibt Girokonten.
 * 
 * @author Albert Wiedemann 
 * @version 1.0
 */
class GIROKONTO extends KONTO
{
    double ueberziehungsrahmen;

    /**
     * Belegt die Attribute vor.
     * @param nummer die Nummer des neuen Kontos
     * @param ueberziehung gew&auml;hrter Dispokredit
     * @param bes Besitzer des Kontos
     * @param ver Verbindungsobjekt zur Datenbank
     */
    GIROKONTO(int nummer, double ueberziehung, KUNDE bes, DATENBANKVERBINDUNG ver)
    {
        super (nummer, bes, ver);
        ueberziehungsrahmen = ueberziehung;
    }

    /**
     * Besetzt die Attribute
     * @param nummer die Nummer des neuen Kontos
     * @param stand der aktuelle Kontostand
     * @param ueberziehung gew&auml;hrter Dispokredit
     * @param bes Besitzer des Kontos
     * @param ver Verbindungsobjekt zur Datenbank
     */
    GIROKONTO (int nummer, double stand, double ueberziehung, KUNDE bes, DATENBANKVERBINDUNG ver)
    {
        super (nummer, stand, bes, ver);
        ueberziehungsrahmen = ueberziehung;
    }
    
    /**
     * Meldet den &Uuml;berziehungsrahmen
     * @return &Uuml;berziehungsrahmen des Kontos
     */
    double UeberziehungsrahmenGeben ()
    {
        return ueberziehungsrahmen;
    }
    
    /**
     * Abheben vom Konto.
     * Voraussetzung: betrag gr&ouml;&szlig;er 0.
     * Die &Auml;nderung wird auch persistent in der Datenbank ausgef&uuml;hrt.
     * Es wird gepr&uuml;ft, dass der Betrag den Kreditrahmen nicht &uuml;bersteigt.
     * @param betrag abzuhebender Betrag
     * @return true, wenn die Aktion ausgef&uuml;hrt werden konnte.
     */
    boolean Abheben (double betrag)
    {
        if (betrag <= kontostand + ueberziehungsrahmen)
        {
            return super. Abheben (betrag);
        }
        else
        {
            return false;
        }
    }
}
