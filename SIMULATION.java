
/**
 * Baut die Simulation auf.
 * 
 * @author Albert Wiedemann 
 * @version 1.0
 */
class SIMULATION
{

    /**
     * Legt die zentralen Objekte an und verkn&uuml;pft sie.
     */
    private SIMULATION()
    {
        KONTROLLEUR k;
        OBERFLAECHE o;
		k = new KONTROLLEUR(BANK. BankGeben ());
		o = new OBERFLAECHE(k);
        //BANK. BankGeben (). DatenbankverbindungGeben (). Registrieren (o);
    }
    
    /**
     * Startmethode
     * @param ags Kommandozeilenargumente
     */
    public static void main (String [] args)
    {
        new SIMULATION ();
    }
}
