
/**
 * Beschreibt die Sicht des Kontrollers f&uuml;r die View.
 * 
 * @author Albert Wiedemann
 * @version 1.0
 */

interface KONTROLLEURINTERFACE
{
    
    /**
     * Setzt die zugeh&ouml;rige Oberfl&auml;che
     * @param o die Oberfl&auml;che
     */
    void OberflaecheSetzen (OBERFLAECHENINTERFACE o);
    
    /**
     * Reagiert auf Beenden-Ereignisse.
     */
    void BeendenAusfuehren ();
    
    /**
     * Abmelden des Kunden oder Angestellten.
     */
    void AbmeldenAusfuehren ();
    
    /**
     * Versucht, den angegebenen Namen als Kunden anzumelden.
     * @param name Name des Kunden
     * @param pin PIN des Kunden
     * @return true, vbei erfolgreicher Anmeldung
     */
    void KundeAnmelden (String name, int pin);

    /**
     * Setzt eine neue PIN f&uuml;r den Kunden.
     * @param nummer die neue PIN
     */
    void KundePinAendern (int nummer);
    
    /**
     * W&auml;hlt das angegebene Konto des aktuellen Kunden.
     * @param nummer: die Nummer des zu w&auml;hlenden Kontos
     */
    void KundeKontoWaehlen (int nummer);
    
    /**
     * Holt die Nummern der verf&uuml;gbaren Konten des aktuellen Kunden.
     * @return die Nummern aller Konten des aktuellen Kunden oder null.
     */
    int [] NummerFuerKundenGeben ();
    
    /**
     * Holt die Nummer des aktuellen Kontos des aktuellen Kunden.
     * @return Nummer des aktuellen Kontos des aktuellen Kunden.
     */
    int NummerFuerKundenKontoGeben ();
    
    /**
     * Holt den verf&uuml;gbaren Betrag des aktuellen Kontos des aktuellen Kunden.
     * @return Betrag auf den aktuellen Konto des aktuellen Kunden.
     */
    double BetragFuerKundenKontoGeben ();
    
    /**
     * Zahlt den angegebenen Betrag auf das aktuelle Konto des Kunden ein.
     */
    void KundeKontoEinzahlen (double betrag);
    
    /**
     * Hebt den angegebenen Betrag vom aktuellen Konto des Kunden ab.
     */
    void KundeKontoAbheben (double betrag);
    
    /**
     * Holt die Kontoausz&uuml;ge des akttuellen Kontos des aktuellen Kunden.
     * @return Feld von Texten mit einem Kontoauszug pro Element
     */
    String [] KundeKontoauszuegeGeben ();
    
    /**
     * L&auml;sst das aktuelle Konto des Kunden neu w&auml;hlen.
     */
    void KundeKontoNeuWaehlen ();
    
    /**
     * Versucht, den angegebenen Namen als Angestellten anzumelden.
     * @param name Name des Angestellten
     * @param pin PIN des Angestellten
     */
    void AngestellterAnmelden (String name, int pin);
    
    /**
     * Meldet die Namen aller Kunden.
     * @return Stringfeld mit den vorhandenen Kundennamen oder null
     */
    String [] KundenNamenGeben ();
    
    /**
     * Meldet die Namen aller Angestellten ausser "Chef" und dem aktuell angemeldeten Angestellten.
     * @return Stringfeld mit den vorhandenen Angestelltennamen oder null
     */
    String [] AngestelltenNamenGeben ();

    /**
     * W&auml;hlt einen Kunden als aktuellen Kunden des Angestellten aus.
     * @param name Name des Kunden
     */
    void AngestellterKundeWaehlen (String name);

    /**
     * Erzeugt einen Kunden und setzt ihn als aktuellen Kunden des Angestellten.
     * @param name Name des Kunden
     * @param pin PIN des Kunden
     */
    void KundeErzeugen (String name, int pin);

    /**
     * Meldet den Namen des aktuellen Kunden.
     * @return name Name des Kunden
     */
    String NameVonAktkundeGeben ();

    /**
     * Erzeugt einen Angestellten.
     * @param name Name des Angestellten
     * @param pin PIN des Angestellten
     */
    void AngestellterErzeugen (String name, int pin);

    
    /**
     * Holt die Nummern der verf&uuml;gbaren Konten des aktuelle bearbeiteten Kunden eines Angestellten.
     */
    int [] NummerFuerAktKundenGeben ();

    /**
     * Legt ein neues Sparkonto f&uuml;r den aktuellen Kunden des Angestellten an.
     * @param zins der Zinssatz f&uuml;r das neue Konto
     */
    void SparkontoEinrichten (double zins);

    /**
     * Legt ein neues Girokonto f&uuml;r den aktuellen Kunden des Angestellten an.
     * @param kredit der &Uuml;berziehungsrahmen f&uuml;r das neue Konto
     */
    void GirokontoEinrichten (double kredit);

    /**
     * Setzt das zu bearbeitende Konto des aktuellen Kunden.
     * @param nummer Nummer des Kontos
     */
    void KontoFuerAngestelltenSetzen (int nummer);

    /**
     * L&ouml;scht das zu bearbeitende Konto des aktuellen Kunden.
     */
    void KontoLoeschen ();
    
    /**
     * Holt die Kontoausz&uuml;ge des aktuellen Kontos des aktuellen Kunden f&uuml;r den Angestellten.
     * @return Feld von Texten mit einem Kontoauszug pro Element
     */
    String [] KontoauszuegeGeben ();
    
    /**
     * Verzinst alle Sparkonten.
     */
    void Verzinsen ();

    /**
     * L&ouml;scht den aktuellen Kunden des aktuellen Angestellten.
     */
    void KundeLoeschen ();
    
    /**
     * L&ouml;scht den angegebenen Angestellten.
     * @param name Name des zu l&ouml;schenden Angestellten.
     */
    void AngestelltenLoeschen (String name);
}
