
/**
 * Verwaltet Listen von KONTEN.
 * 
 * @author Albert Wiedemann 
 * @version 1.0
 */
class KONTENLISTE
{


    /**
     * F&uuml;gt das angegebenen Konto in die Liste ein.
     * @param konto das einzuf&uuml;gende Konto.
     */
    void Einfuegen (KONTO konto)
    {
        
    }

    /**
     * Sucht das Konto mit der angegebenen Nummer.
     * @param nummer die Nummer des zu suchenden Kontos
     * @return Referenz auf das Konto oder null.
     */
    KONTO Suchen (int nummer)
    {
        return null;
    }

    /**
     * L&ouml;scht das angegebene Konto aus der Liste.
     * @param konto das zu l&ouml;schende Konto.
     */
    void Loeschen (KONTO konto)
    {
        
    }

    /**
     * Sucht die Kontonummer f&uuml;r einen bestimmten Kunden in der Datenbank.
     * @param kunde der Kunde dessen Kontonummern gesucht werden sollen.
     * @return Feld mit den Kontonummer
     */
    int [] KontonummernFuerKundenGeben (KUNDE kunde)
    {
        return null;
    }
    
    /**
     * Verzinst alle Sparkonten.
     */
    void Verzinsen ()
    {
        
    }
}
