
/**
 * Beschreibt die Sicht des Kontrollers auf die Oberfl&auml;che.
 * 
 * @author Albert Wiedemann 
 * @version 1.0
 */

interface OBERFLAECHENINTERFACE
{
    /** Die von der Oberfl&auml;che darzustellenden Zust&auml;nde */
    enum Status {anmelden, kundenkontowahl, kundenkontoarbeit, angestelltensicht, angestelltensicht_kunde, angestelltensicht_kundeundkonto};
    
    /**
     * Setzt die Oberfl&auml;che auf den angegebenen Status.
     * @param status der neue Status der Oberfl&auml;che
     */
    void StatusSetzen (Status status);

    /**
     * Statusanzeige besetzen
     * @param text anzuzeigender Statustext
     */
    void StatusmeldungSetzen (String text);

    /**
     * Fehlermeldungen beim Ausf&uuml;hren von Aktionen anzeigen
     * @param text anzuzeigende Fehlermeldung
     */
    void FehlermeldungAnzeigen (String text);

}
