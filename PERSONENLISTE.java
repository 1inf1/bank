
/**
 * Verwaltet Listen von PERSONEN.
 * 
 * @author Albert Wiedemann 
 * @version 1.0
 */
class PERSONENLISTE
{


    /**
     * F&uuml;gt die angegebene Person in die Liste ein.
     * @param person die einzuf&uuml;gende Person.
     */
    void Einfuegen (PERSON person)
    {
        
    }

    /**
     * Sucht die PErson mit dem angegebenen Namen.
     * @param name der Name der zu suchenden Person
     * @return Referenz auf die Person oder null.
     */
    PERSON Suchen (String name)
    {
        return null;
    }

    /**
     * L&ouml;scht die angegebene Person aus der Liste.
     * @param person die zu l&ouml;schende Person.
     */
    void Loeschen (PERSON person)
    {
        
    }
    
    /**
     * Meldet die Namen aller Pesonen in der Liste.
     * @param derNicht diese Person kommt nicht in die Liste.
     * @return Feld mit den Namen der Personen.
     */
    String [] PersonennamenGeben (PERSON derNicht)
    {
        return null;
    }
}
