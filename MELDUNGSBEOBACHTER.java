
/**
 * Beschreibt den Beobachter von Meldungen.
 * 
 * @author  Albert Wiedemann
 * @version 1.0
 */

interface MELDUNGSBEOBACHTER
{
    /**
     * Fehlermeldungen beim Ausf&uuml;hren von Aktionen
     */
    void FehlermeldungEmpfangen (String text);

    /**
     * Aktionslog f&uuml;r den Ablauf
     */
    void LogeintragEmpfangen (String text);

}
